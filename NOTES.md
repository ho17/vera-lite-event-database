Vera Lite device events logging based on CakePHP with Bootstrap
===============================================================

Editing rooms, devices, sections & categories makes no sense, because with a refresh all changes are overwritten. Better to edit in Vera's UI and refresh in the webapp: [https://<your.webserver.ip.address>/sections/refreshAll](https://<your.webserver.ip.address>/sections/refreshAll) .

2014-14-13
----------
- added slider input for dimmable lights
- added support for humidity & temperature sensor

2014-07-02
----------
- added scenes
- added scenes dashboard

2013-12-30
----------
- added controlling devices
- added device status
- added devices dashboard

2013-12-15
----------
- added sections model, view, controller. So import vera.sql
- when a new event of an unknown devices occurs, all data (i.e. rooms, devices, sections & categories) is being refreshed)

2013-12-12
----------
- added graph below device view based on the today events

2013-12-09
----------
- added rooms model, view, controller. So import vera.sql (or update 'devices' table accordingly to keep the current data)
- when a new device is being added, the list with room is also updated.
- possible to manually update rooms list by visiting rooms/refresh in the browser.