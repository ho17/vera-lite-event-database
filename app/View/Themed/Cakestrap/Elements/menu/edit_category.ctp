		<div class="actions">
			
			<ul class="list-group">			
				<li class="list-group-item"><?php echo $this->Html->link(__('List Categories'), array('action' => 'index'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Html->link(__('New Category'), array('action' => 'add'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Form->postLink(__('Delete Category'), array('action' => 'delete', $this->Form->value('Category.id')), array('class' => ''), __('Are you sure you want to delete # %s?', $this->Form->value('Category.id'))); ?> </li>
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
