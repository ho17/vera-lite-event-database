		<div class="actions">
			
			<ul class="list-group">			
				<li class="list-group-item"><?php echo $this->Html->link(__('List Sections'), array('action' => 'index'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Html->link(__('New Section'), array('action' => 'add'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Form->postLink(__('Delete Section'), array('action' => 'delete', $this->Form->value('Section.id')), array('class' => ''), __('Are you sure you want to delete # %s?', $this->Form->value('Section.id'))); ?> </li>
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
