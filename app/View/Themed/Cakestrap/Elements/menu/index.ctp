		<div class="actions">

			<ul class="list-group">
				<li class="list-group-item">
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-home"></span>', array('controller' => 'devices', 'action' => 'dashboard'), array('escape' => FALSE, 'class' => '')); ?>
					<?php echo $this->Html->link(__('Dashboard'), array('controller' => 'devices', 'action' => 'dashboard'), array('class' => '')); ?>
			</ul><!-- /.list-group -->	

			<ul class="list-group">
				<li class="list-group-item">
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-list"></span>', array('controller' => 'events', 'action' => 'index'), array('escape' => FALSE, 'class' => '')); ?>
					<?php echo $this->Html->link(__('Events'), array('controller' => 'events', 'action' => 'index'), array('class' => '')); ?>
				</li>
			</ul><!-- /.list-group -->	

			<ul class="list-group">
				<li class="list-group-item">
						<?php echo $this->Html->link(__('List Sections'), array('controller' => 'sections', 'action' => 'index'), array('class' => '')); ?>
						<?php echo $this->Html->link('<span class="glyphicon glyphicon-refresh btn-sm navbar-right"></span>', array('controller' => 'sections', 'action' => 'refresh'), array('escape' => FALSE, 'class' => '')); ?>
				</li>
				<li class="list-group-item">
					<?php echo $this->Html->link(__('List Rooms'), array('controller' => 'rooms', 'action' => 'index'), array('class' => '')); ?>
											<?php echo $this->Html->link('<span class="glyphicon glyphicon-refresh btn-sm navbar-right"></span>', array('controller' => 'rooms', 'action' => 'refresh'), array('escape' => FALSE, 'class' => '')); ?>
				</li> 
				<li class="list-group-item">
					<?php echo $this->Html->link(__('List Devices'), array('controller' => 'devices', 'action' => 'index'), array('class' => '')); ?>
											<?php echo $this->Html->link('<span class="glyphicon glyphicon-refresh btn-sm navbar-right"></span>', array('controller' => 'devices', 'action' => 'refresh'), array('escape' => FALSE, 'class' => '')); ?>
				</li> 
				<li class="list-group-item">
					<?php echo $this->Html->link(__('List Scenes'), array('controller' => 'scenes', 'action' => 'index'), array('class' => '')); ?>
											<?php echo $this->Html->link('<span class="glyphicon glyphicon-refresh btn-sm navbar-right"></span>', array('controller' => 'scenes', 'action' => 'refresh'), array('escape' => FALSE, 'class' => '')); ?>
				</li> 
				<li class="list-group-item">
					<?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index'), array('class' => '')); ?>
											<?php echo $this->Html->link('<span class="glyphicon glyphicon-refresh btn-sm navbar-right"></span>', array('controller' => 'categories', 'action' => 'refresh'), array('escape' => FALSE, 'class' => '')); ?>
				</li> 
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->