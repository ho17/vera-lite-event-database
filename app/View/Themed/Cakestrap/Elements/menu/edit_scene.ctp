		<div class="actions">
			
			<ul class="list-group">			
				<li class="list-group-item"><?php echo $this->Html->link(__('List Scenes'), array('action' => 'index'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Html->link(__('New Scene'), array('action' => 'add'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Form->postLink(__('Delete Scene'), array('action' => 'delete', $this->Form->value('Scene.id')), array('class' => ''), __('Are you sure you want to delete # %s?', $this->Form->value('Scene.id'))); ?> </li>
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
