		<div class="actions">
			
			<ul class="list-group">
				<li class="list-group-item">
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-home"></span>', array('controller' => 'devices', 'action' => 'dashboard'), array('escape' => FALSE, 'class' => '')); ?>
					<?php echo $this->Html->link(__('Dashboard'), array('controller' => 'devices', 'action' => 'dashboard'), array('class' => '')); ?>
			</ul><!-- /.list-group -->	

			<ul class="list-group">
				<li class="list-group-item">
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-list"></span>', array('controller' => 'events', 'action' => 'index'), array('escape' => FALSE, 'class' => '')); ?>
					<?php echo $this->Html->link(__('Events'), array('controller' => 'events', 'action' => 'index'), array('class' => '')); ?>
				</li>
			</ul><!-- /.list-group -->	

			<ul class="list-group">			
				<li class="list-group-item"><?php echo $this->Html->link(__('List Devices'), array('action' => 'index'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Html->link(__('New Device'), array('action' => 'add'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Html->link(__('Edit Device'), array('action' => 'edit', $device['Device']['id']), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Form->postLink(__('Delete Device'), array('action' => 'delete', $device['Device']['id']), array('class' => ''), __('Are you sure you want to delete # %s?', $device['Device']['id'])); ?> </li>
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
