		<div class="actions">
			
			<ul class="list-group">
				<li class="list-group-item">
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-home"></span>', array('controller' => 'devices', 'action' => 'dashboard'), array('escape' => FALSE, 'class' => '')); ?>
					<?php echo $this->Html->link(__('Dashboard'), array('controller' => 'devices', 'action' => 'dashboard'), array('class' => '')); ?>
			</ul><!-- /.list-group -->	

			<ul class="list-group">
				<li class="list-group-item">
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-list"></span>', array('controller' => 'events', 'action' => 'index'), array('escape' => FALSE, 'class' => '')); ?>
					<?php echo $this->Html->link(__('Events'), array('controller' => 'events', 'action' => 'index'), array('class' => '')); ?>
				</li>
			</ul><!-- /.list-group -->	

			<ul class="list-group">			
				<li class="list-group-item"><?php echo $this->Html->link(__('List Sections'), array('action' => 'index'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Html->link(__('New Section'), array('action' => 'add'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Html->link(__('Edit Section'), array('action' => 'edit', $section['Section']['id']), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Form->postLink(__('Delete Section'), array('action' => 'delete', $section['Section']['id']), array('class' => ''), __('Are you sure you want to delete # %s?', $section['Section']['id'])); ?> </li>
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
