		<div class="actions">
			
			<ul class="list-group">
				<li class="list-group-item">
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-home"></span>', array('controller' => 'devices', 'action' => 'dashboard'), array('escape' => FALSE, 'class' => '')); ?>
					<?php echo $this->Html->link(__('Dashboard'), array('controller' => 'devices', 'action' => 'dashboard'), array('class' => '')); ?>
			</ul><!-- /.list-group -->	

			<ul class="list-group">
				<li class="list-group-item">
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-list"></span>', array('controller' => 'events', 'action' => 'index'), array('escape' => FALSE, 'class' => '')); ?>
					<?php echo $this->Html->link(__('Events'), array('controller' => 'events', 'action' => 'index'), array('class' => '')); ?>
				</li>
			</ul><!-- /.list-group -->	

			<ul class="list-group">			
				<li class="list-group-item"><?php echo $this->Html->link(__('List Rooms'), array('action' => 'index'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Html->link(__('New Room'), array('action' => 'add'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Html->link(__('Edit Room'), array('action' => 'edit', $room['Room']['id']), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Form->postLink(__('Delete Room'), array('action' => 'delete', $room['Room']['id']), array('class' => ''), __('Are you sure you want to delete # %s?', $room['Room']['id'])); ?> </li>
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
