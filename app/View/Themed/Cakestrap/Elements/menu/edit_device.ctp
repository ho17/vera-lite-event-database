		<div class="actions">
			
			<ul class="list-group">			
				<li class="list-group-item"><?php echo $this->Html->link(__('List Devices'), array('action' => 'index'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Html->link(__('New Device'), array('action' => 'add'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Form->postLink(__('Delete Device'), array('action' => 'delete', $this->Form->value('Device.id')), array('class' => ''), __('Are you sure you want to delete # %s?', $this->Form->value('Device.id'))); ?> </li>
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
