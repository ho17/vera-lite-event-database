		<div class="actions">
			
			<ul class="list-group">			
				<li class="list-group-item"><?php echo $this->Html->link(__('List Rooms'), array('action' => 'index'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Html->link(__('New Room'), array('action' => 'add'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Form->postLink(__('Delete Room'), array('action' => 'delete', $this->Form->value('Room.id')), array('class' => ''), __('Are you sure you want to delete # %s?', $this->Form->value('Room.id'))); ?> </li>
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
