			<ul class="list-group">
				<?php 
					if ($this->Session->read('Auth.User')){
				?>
					<li class="list-group-item"><?php echo $this->Html->link(__('Logout', true), array('controller' => 'users', 'action' => 'logout'), array('title' => 'Logout')); ?></li>
				<?php 
					} else {
				 ?>
					<li class="list-group-item"><?php echo $this->Html->link(__('Login', true), array('controller' => 'users', 'action' => 'login'), array('title' => 'Login')); ?></li>
				<?php 
					}
				?>
			</ul><!-- /.list-group -->