<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

?>
<?php echo $this->Html->docType('html5'); ?> 
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>
			<?php echo $title_for_layout; ?>
		</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php
			echo $this->Html->meta('icon');
			
			echo $this->fetch('meta');

			echo $this->Html->css('bootstrap');
			echo $this->Html->css('main');
			echo $this->Html->css('datepicker');
			echo $this->Html->css('bootstrap-slider');

			echo $this->fetch('css');
			
			echo $this->Html->script('jquery-2.0.3.min');
			echo $this->Html->script('jquery-ui-1.10.3.custom.min');
			echo $this->Html->script('bootstrap.min');
			echo $this->Html->script('bootstrap-datepicker');
			echo $this->Html->script('bootstrap-slider');

			echo $this->Html->script('canvasjs.min'); // graph library
			echo $this->Html->script('raphael-min'); // svg library
			echo $this->Html->script('jquery.polartimer');
			
			echo $this->fetch('script');
		?>
	</head>

	<body>

		<div id="main-container">
<!-- 
			<div id="header" class="container">
				<?php //echo $this->element('menu/top_menu'); ?>
			</div>
-->			
			<div id="content" class="container">
				<?php echo $this->Session->flash(); ?>
				<?php echo $this->Session->flash('auth'); ?>
				<?php echo $this->fetch('content'); ?>
			</div><!-- /#content .container -->
<!--			
			<div id="footer" class="container">
				<?php //Silence is golden ?>
			</div>
-->
			
		</div><!-- /#main-container -->
		
	</body>

</html>