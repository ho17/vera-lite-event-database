<script type="text/javascript">
	$( document ).ready(function() {
		updateStatus();

		// update device status regularly
		$('#progress').polartimer({
		 timerSeconds: <?php echo REFRESH_STATUS; ?>,
		 color: '#DDDDDD',
		 opacity: 0.5,
		 callback: function () {
			updateStatus();
			$('#progress').polartimer('start');
		 }
		});
		$('#progress').polartimer('start');
	});

	var updateStatus = function() {
	    <?php
			echo $this->Js->request(
		        array('controller' => 'devices', 'action' => 'status'),
		        array('async' => true,
		        	  'update' => '#devices_success',
		        	));
		?>
	};
</script>

<div class='hidden' id='devices_success'></div>

<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<?php echo $this->element('menu/view_category'); ?>

		<div id="progress"></div>
		
	</div><!-- /#sidebar .span3 -->
	
	<div id="page-content" class="col-sm-9">
		
		<div class="categories view">

			<div class="page-header">
				<h2><?php  echo __('Category'); ?></h2>
			</div>
			
			<div class="table-responsive">
				<table class="table table-hover table-striped table-bordered <?php if (TABLE_CONDENSED) { echo 'table-condensed'; } ?>">
					<tbody>
						<tr>
							<td><strong><?php echo __('Id'); ?></strong></td>
							<td>
								<?php echo h($category['Category']['id']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Name'); ?></strong></td>
							<td>
								<?php echo h($category['Category']['name']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Created'); ?></strong></td>
							<td>
								<?php echo h($category['Category']['created']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Modified'); ?></strong></td>
							<td>
								<?php echo h($category['Category']['modified']); ?>
								&nbsp;
							</td>
						</tr>
					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

					
		<div class="related">
			<?php if (!empty($category['Device'])): ?>
			<h3><?php echo __('Related Devices'); ?></h3>
				
			<div class="table-responsive">
				<table class="table table-hover table-striped table-bordered <?php if (TABLE_CONDENSED) { echo 'table-condensed'; } ?>">
					<thead>
						<tr>
							<th><?php echo __('Id'); ?></th>
							<th><?php echo __('Name'); ?></th>
							<th><?php echo __('Room'); ?></th>
							<th><?php echo __('Status'); ?></th>
							<th><?php echo __('Created'); ?></th>
							<th><?php echo __('Modified'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php
						foreach ($category['Device'] as $device):
					?>

						<tr>
							<td><?php echo $device['id']; ?><span class='hidden' id='device_previous_status_<?php echo h($device['id']); ?>'></span></td>
							<td><?php echo $device['name']; ?></td>
							<td><?php echo $rooms[$device['room_id']]; ?></td>
							<td id="device_current_status_<?php echo h($device['id']); ?>">&nbsp;</td>			
							<td><?php echo $device['created']; ?></td>
							<td><?php echo $device['modified']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('View'), array('controller' => 'devices', 'action' => 'view', $device['id']), array('class' => 'btn btn-default btn-xs')); ?>
									<?php echo $this->Html->link(__('Edit'), array('controller' => 'devices', 'action' => 'edit', $device['id']), array('class' => 'btn btn-default btn-xs')); ?>
									<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'devices', 'action' => 'delete', $device['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $device['id'])); ?>
									<?php
										if (in_array($device['category_id'], array(CAT_DIMMABLE_LIGHT, CAT_SWITCH, CAT_WINDOW_COVERING))) {
											$btn1Label = 'On';
											$btn0Label = 'Off';
											$btn1TargetValue = 1;
											$btn0TargetValue = 0;

											switch ($device['category_id']) {
												case CAT_WINDOW_COVERING:
													$btn1Label = 'Open';
													$btn0Label = 'Close';
													break;
												case CAT_DIMMABLE_LIGHT:
													$btn1TargetValue = 100;
													break;
											}

											echo $this->Js->link(
											    $btn1Label,
											    array('controller' => 'devices', 'action' => 'toggle', h($device['id']), $btn1TargetValue),
											    array('async' => true, 
											    	  'htmlAttributes' => array('class' => 'btn btn-default btn-xs')
											    	)
											);

											echo "\n"; // create white space between buttons

											echo $this->Js->link(
											    $btn0Label,
											    array('controller' => 'devices', 'action' => 'toggle', h($device['id']), $btn0TargetValue),
											    array('async' => true, 
											    	  'htmlAttributes' => array('class' => 'btn btn-default btn-xs')
											    	)
											);

											echo $this->Js->writeBuffer();					
										}
									?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
		
			<?php endif; ?>
				
			<div class="actions">
				<?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Device'), array('controller' => 'devices', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
			</div><!-- /.actions -->
		
		</div><!-- /.related -->

	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->