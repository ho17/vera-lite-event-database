<?php
//	$this->Html->scriptStart(array('inline' => false));
//	$this->Html->scriptEnd();
?>

  <script type="text/javascript">
    var chart = new CanvasJS.Chart("chartForDeviceId<?php echo $deviceId; ?>", {
      axisY:{
        maximum: 1.1,
    	minimum: 0,
    	gridThickness: 1,
    	interval: 1,
      },
	  axisX:{
        valueFormatString: "H:mm", 
        interval: 1,
        intervalType: "hour",
      },
      data: [//array of dataSeries              
	        { //dataSeries object

	         type: "stepLine",
	         lineThickness: 3,
	         xValueType: "dateTime",
	         dataPoints: [
<?php
				foreach ($events as $event):

					$date = DateTime::createFromFormat('Y-m-d H:i:s', $event['Event']['created']);

					$year = $date->format('Y');
					$month = $date->format('m');
					$day = $date->format('d');
					$hours = $date->format('H');
					$minutes = $date->format('i');
					$secondes = $date->format('s');
					$value = $event['Event']['value'];
					printf("{ x: new Date(%s,%s,%s,%s,%s,%s), y: %s },", $year, $month, $day, $hours, $minutes, $secondes, $value);

				endforeach;
?>	         
	         ]
	       }
       ]
     });

    chart.render();
  </script>

<div id="chartForDeviceId<?php echo $deviceId; ?>" style="height: 300px; width: 100%;">
</div>