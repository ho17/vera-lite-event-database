<?php
	function getStatusText($deviceCategory, $deviceStatus, $deviceStatusLevel) {
		$statusText = 'Undefined';
		switch ($deviceCategory) {
		 	case CAT_DIMMABLE_LIGHT:
				if (($deviceStatusLevel > 0) && ($deviceStatusLevel < 100)) {
		 			$statusText = $deviceStatusLevel . "%";
		 		} else {
		 			$statusText = ($deviceStatusLevel == 100 ? "On" : "Off");
		 		}
		 		break;
		 	case CAT_SWITCH:
		 		$statusText = ($deviceStatus == 1 ? "On" : "Off");
		 		break;
		 	case CAT_WINDOW_COVERING:
				if (($deviceStatusLevel > 0) && ($deviceStatusLevel < 100)) {
		 			$statusText = $deviceStatus . "% Open";
		 		} else {
		 			$statusText = ($deviceStatus == 1 ? "Opened" : "Closed");
		 		}
		 		break;
		 	case CAT_SENSOR:
		 		$statusText = ($deviceStatus == 1 ? "Tripped" : "Not Tripped");
		 		break;
			case CAT_HUMIDITY_SENSOR:
		 		$statusText = $deviceStatus . "%";
		 		break;
		 	case CAT_TEMPERATURE_SENSOR:
		 		$statusText = $deviceStatus . "°C";
		 		break;
		 	default:
		 		$statusText = $deviceStatus;
		 		break;
		 }

		 return $statusText;
	}

?>

<script type="text/javascript">
	var showStatus = function( id, statusValue, statusText ) {
		var c = (statusValue === '0') ? '#F2DEDE' : '#DFF0D8';
		var previousStatusText = $('#device_previous_status_' + id).text();
		if (previousStatusText != statusText) {
			$('#device_previous_status_' + id).html(statusText);
			$('#device_current_status_' + id).html(statusText);
			$('#device_current_status_' + id).effect("highlight", {color: c}, 3500);
		}
	};

	<?php
		foreach ($devicesStatus as $id => $status) {
	?>
			showStatus(<?php echo $id; ?>, 
						'<?php echo $status['status'] ?>', 
						'<?php echo getStatusText($status['categoryId'],
												  $status['status'],
												  $status['statusLevel']
												  ); ?>'
						);
	<?php
			if ($status['categoryId'] == CAT_DIMMABLE_LIGHT) {
				?>
					if( $('#dimmable_<?php echo $id; ?>').length ) { // element exists
						$('#dimmable_<?php echo $id; ?>').slider('setValue', <?php echo $status['statusLevel']; ?>);
					}
				<?php
			}
		}
	?>
</script>