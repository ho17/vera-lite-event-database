<script type="text/javascript">
	$( document ).ready(function() {
		updateStatus();

		// update device status regularly
		$('#progress').polartimer({
		 timerSeconds: <?php echo REFRESH_STATUS; ?>,
		 color: '#DDDDDD',
		 opacity: 0.5,
		 callback: function () {
			updateStatus();
			$('#progress').polartimer('start');
		 }
		});
		$('#progress').polartimer('start');
	});

	var updateStatus = function() {
	    <?php
			echo $this->Js->request(
		        array('controller' => 'devices', 'action' => 'status'),
		        array('async' => true,
		        	  'update' => '#devices_success',
		        	));
		?>
	};
</script>

<div class='hidden' id='devices_success'></div>

<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<?php echo $this->element('menu/index'); ?>

		<div id="progress"></div>
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<div class="devices index">
		
			<div class="page-header">
				<h2><?php echo __('Devices'); ?></h2>
			</div>
			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-striped table-bordered <?php if (TABLE_CONDENSED) { echo 'table-condensed'; } ?>">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('id'); ?></th>
							<th><?php echo $this->Paginator->sort('name'); ?></th>
							<th><?php echo $this->Paginator->sort('room_id'); ?></th>
							<th><?php echo $this->Paginator->sort('category_id'); ?></th>
							<th><?php echo __('Status'); ?></th>
							<th><?php echo $this->Paginator->sort('on_dashboard'); ?></th>
							<th><?php echo $this->Paginator->sort('created'); ?></th>
							<th><?php echo $this->Paginator->sort('modified'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($devices as $device): ?>
						<tr>
							<td><?php echo h($device['Device']['id']); ?>&nbsp;<span class='hidden' id='device_previous_status_<?php echo h($device['Device']['id']); ?>'></span></td>
							<td><?php echo h($device['Device']['name']); ?>&nbsp;</td>
							<td>
								<?php echo $this->Html->link($device['Room']['name'], array('controller' => 'rooms', 'action' => 'view', $device['Room']['id'])); ?>
							</td>
							<td>
								<?php echo $this->Html->link($device['Category']['name'], array('controller' => 'categories', 'action' => 'view', $device['Category']['id'])); ?>
							</td>
							<td id="device_current_status_<?php echo h($device['Device']['id']); ?>">&nbsp;</td>
							<td><?php echo h($device['Device']['on_dashboard']); ?>&nbsp;</td>
							<td><?php echo h($device['Device']['created']); ?>&nbsp;</td>
							<td><?php echo h($device['Device']['modified']); ?>&nbsp;</td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('View'), array('action' => 'view', $device['Device']['id']), array('class' => 'btn btn-default btn-xs')); ?>
									<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $device['Device']['id']), array('class' => 'btn btn-default btn-xs')); ?>
									<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $device['Device']['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $device['Device']['id'])); ?>
									<?php
										if (in_array($device['Category']['id'], array(CAT_DIMMABLE_LIGHT, CAT_SWITCH, CAT_WINDOW_COVERING))) {
											$btn1Label = 'On';
											$btn0Label = 'Off';
											$btn1TargetValue = 1;
											$btn0TargetValue = 0;

											switch ($device['Category']['id']) {
												case CAT_WINDOW_COVERING:
													$btn1Label = 'Open';
													$btn0Label = 'Close';
													break;
												case CAT_DIMMABLE_LIGHT:
													$btn1TargetValue = 100;
													break;
											}

											echo $this->Js->link(
											    $btn1Label,
											    array('controller' => 'devices', 'action' => 'toggle', h($device['Device']['id']), $btn1TargetValue),
											    array('async' => true, 
											    	  'htmlAttributes' => array('class' => 'btn btn-default btn-xs')
											    	)
											);

											echo "\n"; // create white space between buttons

											echo $this->Js->link(
											    $btn0Label,
											    array('controller' => 'devices', 'action' => 'toggle', h($device['Device']['id']), $btn0TargetValue),
											    array('async' => true, 
											    	  'htmlAttributes' => array('class' => 'btn btn-default btn-xs')
											    	)
											);

											echo $this->Js->writeBuffer();
										}
									?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			<p><small>
				<?php
					echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					));
				?>
			</small></p>

			<ul class="pagination">
				<?php
					echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
					echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
					echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				?>
			</ul><!-- /.pagination -->
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->