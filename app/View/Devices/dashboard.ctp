<script type="text/javascript">
	$( document ).ready(function() {
		updateStatus();

		// update device status regularly
		setInterval(function() {
		    updateStatus();
		}, <?php echo REFRESH_STATUS*1000; ?>);

		// Resize the devices to the same height
		boxes = $('.device');
		maxHeight = Math.max.apply(
		Math, boxes.map(function() {
		    return $(this).height();
		}).get());
		boxes.height(maxHeight);

		<?php
		foreach ($devices as $device):
			if ($device['Category']['id']==CAT_DIMMABLE_LIGHT) {
		?>
				if( $('#dimmable_<?php echo h($device['Device']['id']); ?>').length ) { // element exists
					// Initialize slider
					$('#dimmable_<?php echo h($device['Device']['id']); ?>').slider({
						formater: function(value) {
							return 'Current value: ' + value;
						}
					});

					$('#dimmable_<?php echo h($device['Device']['id']); ?>').on('slideStop', function(slideEvt) {
					    $.ajax({async:true, url:"\/devices\/toggle\/32\/" + slideEvt.value});
					});
				}
		<?php
			}
		endforeach;
		?>
	});

	var updateStatus = function() {
	    <?php
			echo $this->Js->request(
		        array('controller' => 'devices', 'action' => 'status'),
		        array('async' => true,
		        	  'update' => '#devices_success',
		        	));
		?>
	};
</script>

<div class='hidden' id='devices_success'></div>

<?php
	$this->set('title_for_layout', "Dashboard");

	$old_room_id = -1;
	$i = 0;
?>

<ul class="nav nav-tabs nav-justified">
  <li class="active"><a href="#">Devices</a></li>
  <li><?php echo $this->Html->link(__('Scenes'), array('controller' => 'scenes', 'action' => 'dashboard'), array('class' => '')); ?></li>
</ul>

<br>

<div class="row alternate_1">
	<?php

		foreach ($devices as $device):
			if ($old_room_id != $device['Room']['id']) {
				// New room, new row
				$old_room_id = $device['Room']['id'];
				$i++;
				if ($i>1) {
	?>
					</div>
					<div class="row alternate_<?php echo ($i % 2); ?>">
	<?php
				}
			}
	?>
		<div class="col-xs-6 col-md-3 col-lg-2">
		    <div class="thumbnail device device_id_<?php echo $device['Device']['id']; ?> room_id_<?php echo $device['Room']['id']; ?> room_color_<?php echo ($device['Room']['id'] % 10)+1; ?>">

		      	<div class="caption">
			        <h6><?php echo h($device['Room']['name']); ?></h6>		        
			        <h4><?php echo h($device['Device']['name']); ?></h4>
			        <span class='hidden' id='device_previous_status_<?php echo h($device['Device']['id']); ?>'></span>
			        <p id="device_current_status_<?php echo h($device['Device']['id']); ?>">&nbsp;</p>
					<?php
						if (in_array($device['Category']['id'], array(CAT_DIMMABLE_LIGHT, CAT_SWITCH, CAT_WINDOW_COVERING))) {
					?>
			        <p class="text-center bottom">
					<?php
							$btn1Label = 'On';
							$btn0Label = 'Off';
							$btn1TargetValue = 1;
							$btn0TargetValue = 0;

							switch ($device['Category']['id']) {
								case CAT_WINDOW_COVERING:
									$btn1Label = 'Open';
									$btn0Label = 'Close';
									break;
								case CAT_DIMMABLE_LIGHT:
									$btn1TargetValue = 100;
									break;
							}

							if ($device['Category']['id']==CAT_DIMMABLE_LIGHT) {
								echo $this->Form->input('dimmable_' . $device['Device']['id'], array(
									'div' => false,
									'label' => false,
								    'data-slider-id' => 'ex1Slider',
								    'data-slider-min' => '0',
								    'data-slider-max' => '100',
								    'data-slider-step' => DIMMABLE_LIGHT_STEP_SIZE,
								    'data-slider-value' => '0',
								));
							} else {
								echo $this->Js->link(
								    $btn1Label,
								    array('action' => 'toggle', h($device['Device']['id']), $btn1TargetValue),
								    array('async' => true, 
								    	  'htmlAttributes' => array('class' => 'btn btn-success')
								    	)
								);

								echo "\n"; // create white space between buttons

								echo $this->Js->link(
								    $btn0Label,
								    array('action' => 'toggle', h($device['Device']['id']), $btn0TargetValue),
								    array('async' => true, 
								    	  'htmlAttributes' => array('class' => 'btn btn-danger')
								    	)
								);
							}

							echo $this->Js->writeBuffer();
					?>
			        </p>
					<?php
						}
					?>
		        
		      	</div>
		    </div>
		</div>
	<?php endforeach; ?>
</div>