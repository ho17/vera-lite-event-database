<script type="text/javascript">
	$( document ).ready(function() {
		updateStatus();
		<?php if (!empty($events)): ?>
			updateGraph();
		<?php endif; ?>

		// update device status regularly
		$('#progress').polartimer({
		 timerSeconds: <?php echo REFRESH_STATUS; ?>,
		 color: '#DDDDDD',
		 opacity: 0.5,
		 callback: function () {
			updateStatus();
			$('#progress').polartimer('start');
		 }
		});
		$('#progress').polartimer('start');

		<?php
			if ($device['Category']['id']==CAT_DIMMABLE_LIGHT) {
		?>
				if( $('#dimmable_<?php echo h($device['Device']['id']); ?>').length ) { // element exists
					// Initialize slider
					$('#dimmable_<?php echo h($device['Device']['id']); ?>').slider({
						formater: function(value) {
							return 'Current value: ' + value;
						}
					});

					$('#dimmable_<?php echo h($device['Device']['id']); ?>').on('slideStop', function(slideEvt) {
					    $.ajax({async:true, url:"\/devices\/toggle\/32\/" + slideEvt.value});
					});
				}
		<?php
			}
		?>
	});

	var updateStatus = function() {
	    <?php
			echo $this->Js->request(
		        array('controller' => 'devices', 'action' => 'status', h($device['Device']['id'])),
		        array('async' => true,
		        	  'update' => '#devices_success',
		        	));
		?>
	};

	var updateGraph = function() {
		<?php
			echo $this->Js->request(
				array('action' => 'today', h($device['Device']['id'])),
				array('async' => true,
		  			  'update' => '#graph_success_' . h($device['Device']['id']),
					 ));
		?>
	}
</script>

<div class='hidden' id='devices_success'></div>
<div class='hidden' id='device_previous_status_<?php echo h($device['Device']['id']); ?>'></div>

<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<?php echo $this->element('menu/view_device'); ?>

		<div id="progress"></div>
	
	</div><!-- /#sidebar .span3 -->


	<div id="page-content" class="col-sm-9">
		
		<div class="devices view">

			<div class="page-header">
			  <h2><?php echo __('Device'); ?></h2>
			</div>

			<div class="table-responsive">
				<table class="table table-hover table-striped table-bordered <?php if (TABLE_CONDENSED) { echo 'table-condensed'; } ?>">
					<tbody>
						<tr>
							<td><strong><?php echo __('Id'); ?></strong></td>
							<td>
								<?php echo h($device['Device']['id']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Name'); ?></strong></td>
							<td>
								<?php echo h($device['Device']['name']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Room'); ?></strong></td>
							<td>
								<?php echo $this->Html->link($device['Room']['name'], array('controller' => 'rooms', 'action' => 'view', $device['Room']['id']), array('class' => '')); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Category'); ?></strong></td>
							<td>
								<?php echo $this->Html->link($device['Category']['name'], array('controller' => 'categories', 'action' => 'view', $device['Category']['id']), array('class' => '')); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Show on dashboard'); ?></strong></td>
							<td>
								<?php echo h($device['Device']['on_dashboard']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Created'); ?></strong></td>
							<td>
								<?php echo h($device['Device']['created']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Modified'); ?></strong></td>
							<td>
								<?php echo h($device['Device']['modified']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Current status'); ?></strong></td>
							<td id="device_current_status_<?php echo h($device['Device']['id']); ?>">
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Events today'); ?></strong></td>
							<td>
								<?php echo h($dayCount); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Events this week'); ?></strong></td>
							<td>
								<?php echo h($weekCount); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Events this month'); ?></strong></td>
							<td>
								<?php echo h($monthCount); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Events this year'); ?></strong></td>
							<td>
								<?php echo h($yearCount); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Total events'); ?></strong></td>
							<td>
								<?php echo h($totalCount); ?>
								&nbsp;
							</td>
						</tr>
					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

		<div class="actions">
			<?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Event'), array('controller' => 'events', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>

			<?php
				if (in_array($device['Category']['id'], array(CAT_DIMMABLE_LIGHT, CAT_SWITCH, CAT_WINDOW_COVERING))) {
					$btn1Label = 'On';
					$btn0Label = 'Off';
					$btn1TargetValue = 1;
					$btn0TargetValue = 0;

					switch ($device['Category']['id']) {
						case CAT_WINDOW_COVERING:
							$btn1Label = 'Open';
							$btn0Label = 'Close';
							break;
						case CAT_DIMMABLE_LIGHT:
							$btn1TargetValue = 100;
							break;
					}

					echo $this->Js->link(
					    $btn1Label,
					    array('action' => 'toggle', h($device['Device']['id']), $btn1TargetValue),
					    array('async' => true, 
					    	  'htmlAttributes' => array('class' => 'btn btn-primary')
					    	)
					);

					echo "\n"; // create white space between buttons

					echo $this->Js->link(
					    $btn0Label,
					    array('action' => 'toggle', h($device['Device']['id']), $btn0TargetValue),
					    array('async' => true, 
					    	  'htmlAttributes' => array('class' => 'btn btn-primary')
					    	)
					);

					if ($device['Category']['id']==CAT_DIMMABLE_LIGHT) {
						echo $this->Form->input('dimmable_' . $device['Device']['id'], array(
							'div' => false,
							'label' => false,
						    'data-slider-id' => 'ex1Slider',
						    'data-slider-min' => '0',
						    'data-slider-max' => '100',
						    'data-slider-step' => DIMMABLE_LIGHT_STEP_SIZE,
						    'data-slider-value' => '0',
						));
					}

					echo $this->Js->writeBuffer();
				}
			?>
		</div><!-- /.actions -->

		<?php if (!empty($events)): ?>

			<div class="related">

				<h3><?php echo __('All related Events'); ?></h3>
						
                    <div class="table-responsive">
                    	<table class="table table-hover table-striped table-bordered <?php if (TABLE_CONDENSED) { echo 'table-condensed'; } ?>">
                    		<thead>
								<tr>
									<th><?php echo $this->Paginator->sort('Event.id', 'Id'); ?></th>
									<th><?php echo $this->Paginator->sort('Event.device_id', 'Device Id'); ?></th>
									<th><?php echo $this->Paginator->sort('Event.description', 'Description'); ?></th>
									<th><?php echo $this->Paginator->sort('Event.value', 'Value'); ?></th>
									<th><?php echo $this->Paginator->sort('Event.created', 'Created'); ?></th>
									<th><?php echo $this->Paginator->sort('Event.modified', 'Modified'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
							<?php
								$i = 0;
								foreach ($events as $event):
									$event = $event["Event"];
							?>
									<tr>
										<td><?php echo $event['id']; ?></td>
										<td><?php echo $event['device_id']; ?></td>
										<td><?php echo $event['description']; ?></td>
										<td><?php echo $event['value']; ?></td>
										<td><?php echo $event['created']; ?></td>
										<td><?php echo $event['modified']; ?></td>
										<td class="actions">
											<div class="btn-group">
												<?php echo $this->Html->link(__('View'), array('controller' => 'events', 'action' => 'view', $event['id']), array('class' => 'btn btn-default btn-xs')); ?>
												<?php echo $this->Html->link(__('Edit'), array('controller' => 'events', 'action' => 'edit', $event['id']), array('class' => 'btn btn-default btn-xs')); ?>
												<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'events', 'action' => 'delete', $event['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $event['id'])); ?>
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table><!-- /.table table-striped table-bordered -->
					</div><!-- /.table-responsive -->

					<p><small>
						<?php
							echo $this->Paginator->counter(array(
							'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
							));
						?>
					</small></p>

					<ul class="pagination">
						<?php
							echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
							echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
							echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
						?>
					</ul><!-- /.pagination -->
					
					<h3><?php echo __('Today events'); ?></h3>

					<div class="graph" id="graph_success_<?php echo h($device['Device']['id']); ?>">
						<!-- graph container, graph inserted by ajax on document ready -->
					</div>
				
				</div><!-- /.related -->

		<?php endif; ?>
			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->