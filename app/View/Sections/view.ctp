
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<?php echo $this->element('menu/view_section'); ?>
		
	</div><!-- /#sidebar .span3 -->
	
	<div id="page-content" class="col-sm-9">
		
		<div class="sections view">

			<div class="page-header">
				<h2><?php  echo __('Section'); ?></h2>
			</div>
			
			<div class="table-responsive">
				<table class="table table-striped table-bordered <?php if (TABLE_CONDENSED) { echo 'table-condensed'; } ?>">
					<tbody>
						<tr>
							<td><strong><?php echo __('Id'); ?></strong></td>
							<td>
								<?php echo h($section['Section']['id']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Name'); ?></strong></td>
							<td>
								<?php echo h($section['Section']['name']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Created'); ?></strong></td>
							<td>
								<?php echo h($section['Section']['created']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Modified'); ?></strong></td>
							<td>
								<?php echo h($section['Section']['modified']); ?>
								&nbsp;
							</td>
						</tr>
					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->
					
		<div class="related">

			<h3><?php echo __('Related Rooms'); ?></h3>
				
			<?php if (!empty($section['Room'])): ?>
					
				<div class="table-responsive">
					<table class="table table-striped table-bordered <?php if (TABLE_CONDENSED) { echo 'table-condensed'; } ?>">
						<thead>
							<tr>
								<th><?php echo __('Id'); ?></th>
								<th><?php echo __('Name'); ?></th>
								<th><?php echo __('Section Id'); ?></th>
								<th><?php echo __('Created'); ?></th>
								<th><?php echo __('Modified'); ?></th>
								<th class="actions"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php
							foreach ($section['Room'] as $room):
						?>
							<tr>
								<td><?php echo $room['id']; ?></td>
								<td><?php echo $room['name']; ?></td>
								<td><?php echo $room['section_id']; ?></td>
								<td><?php echo $room['created']; ?></td>
								<td><?php echo $room['modified']; ?></td>
								<td class="actions">
									<?php echo $this->Html->link(__('View'), array('controller' => 'rooms', 'action' => 'view', $room['id']), array('class' => 'btn btn-default btn-xs')); ?>
									<?php echo $this->Html->link(__('Edit'), array('controller' => 'rooms', 'action' => 'edit', $room['id']), array('class' => 'btn btn-default btn-xs')); ?>
									<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'rooms', 'action' => 'delete', $room['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $room['id'])); ?>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table><!-- /.table table-striped table-bordered -->
				</div><!-- /.table-responsive -->
					
			<?php endif; ?>

			<div class="actions">
				<?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Room'), array('controller' => 'rooms', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
			</div><!-- /.actions -->
				
		</div><!-- /.related -->
			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
