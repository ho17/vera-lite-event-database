
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<?php echo $this->element('menu/view_event'); ?>
		
	</div><!-- /#sidebar .span3 -->
	
	<div id="page-content" class="col-sm-9">
		
		<div class="events view">

			<div class="page-header">
				<h2><?php  echo __('Event'); ?></h2>
			</div>
			
			<div class="table-responsive">
				<table class="table table-hover table-striped table-bordered <?php if (TABLE_CONDENSED) { echo 'table-condensed'; } ?>">
					<tbody>
						<tr>
							<td><strong><?php echo __('Id'); ?></strong></td>
							<td>
								<?php echo h($event['Event']['id']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Device'); ?></strong></td>
							<td>
								<?php echo $this->Html->link($event['Device']['name'], array('controller' => 'devices', 'action' => 'view', $event['Device']['id']), array('class' => '')); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Description'); ?></strong></td>
							<td>
								<?php echo h($event['Event']['description']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Value'); ?></strong></td>
							<td>
								<?php echo h($event['Event']['value']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Created'); ?></strong></td>
							<td>
								<?php echo h($event['Event']['created']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Modified'); ?></strong></td>
							<td>
								<?php echo h($event['Event']['modified']); ?>
								&nbsp;
							</td>
						</tr>
					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->
			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
