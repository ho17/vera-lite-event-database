<script type="text/javascript">
	$( document ).ready(function() {
		updateStatus();

		// update device status regularly
		setInterval(function() {
		    updateStatus();
		}, <?php echo REFRESH_STATUS*1000; ?>);

		// Resize the scenes to the same height
		boxes = $('.device');
		maxHeight = Math.max.apply(
		Math, boxes.map(function() {
		    return $(this).height();
		}).get());
		boxes.height(maxHeight);
	});

	var updateStatus = function() {
	    <?php
			echo $this->Js->request(
		        array('controller' => 'scenes', 'action' => 'status'),
		        array('async' => true,
		        	  'update' => '#scenes_success',
		        	));
		?>
	};
</script>

<div class='hidden' id='scenes_success'></div>

<?php
	$this->set('title_for_layout', "Dashboard");

	$old_room_id = -1;
	$i = 0;
?>

<ul class="nav nav-tabs nav-justified">
  <li><?php echo $this->Html->link(__('Devices'), array('controller' => 'devices', 'action' => 'dashboard'), array('class' => '')); ?></li>
  <li class="active"><a href="#">Scenes</a></li>
</ul>

<br>

<div class="row alternate_1">
	<?php

		foreach ($scenes as $scene):
			if ($old_room_id != $scene['Room']['id']) {
				// New room, new row
				$old_room_id = $scene['Room']['id'];
				$i++;
				if ($i>1) {
	?>
					</div>
					<div class="row alternate_<?php echo ($i % 2); ?>">
	<?php
				}
			}
	?>
		<div class="col-xs-6 col-md-3 col-lg-2">
		    <div class="thumbnail scene scene_id_<?php echo $scene['Scene']['id']; ?> room_id_<?php echo $scene['Room']['id']; ?> scene_color_<?php echo ($scene['Room']['id'] % 10)+1; ?>">

		     	<div class="caption">
			        <h6><?php echo h($scene['Room']['name']); ?></h6>
			        <h4><?php echo h($scene['Scene']['name']); ?></h4>
			        <span class='hidden' id='scene_previous_status_<?php echo h($scene['Scene']['id']); ?>'></span>
			        <p id="scene_current_status_<?php echo h($scene['Scene']['id']); ?>">&nbsp;</p>
			        <p class="text-center bottom">
					<?php
						echo $this->Js->link(
						    'Run',
						    array('action' => 'run', $scene['Scene']['id']),
						    array('async' => true, 
						    	  'htmlAttributes' => array('class' => 'btn btn-success')
						    	)
						);

						echo $this->Js->writeBuffer();
					?>
			        </p>
		      	</div>
		    </div>
		</div>
	<?php endforeach; ?>
</div>