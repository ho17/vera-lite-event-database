<script type="text/javascript">
	$( document ).ready(function() {
		updateActive();

		// update device status regularly
		$('#progress').polartimer({
		 timerSeconds: <?php echo REFRESH_ACTIVE; ?>,
		 color: '#DDDDDD',
		 opacity: 0.5,
		 callback: function () {
			updateActive();
			$('#progress').polartimer('start');
		 }
		});
		$('#progress').polartimer('start');
	});

	var updateActive = function() {
	    <?php
			echo $this->Js->request(
		        array('controller' => 'scenes', 'action' => 'status'),
		        array('async' => true,
		        	  'update' => '#scenes_success',
		        	));
		?>
	};
</script>

<div class='hidden' id='scenes_success'></div>

<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">

		<?php echo $this->element('menu/index'); ?>

		<div id="progress"></div>
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<div class="scenes index">

			<div class="page-header">
				<h2><?php echo __('Scenes'); ?></h2>
			</div>
			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-striped table-bordered <?php if (TABLE_CONDENSED) { echo 'table-condensed'; } ?>">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('id'); ?></th>
							<th><?php echo $this->Paginator->sort('name'); ?></th>
							<th><?php echo $this->Paginator->sort('room_id'); ?></th>
							<th><?php echo __('Status'); ?></th>
							<th><?php echo $this->Paginator->sort('on_dashboard'); ?></th>
							<th><?php echo $this->Paginator->sort('created'); ?></th>
							<th><?php echo $this->Paginator->sort('modified'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($scenes as $scene): ?>
						<tr>
							<td><?php echo h($scene['Scene']['id']); ?>&nbsp;<span class='hidden' id='scene_previous_status_<?php echo h($scene['Scene']['id']); ?>'></span></td>
							<td><?php echo h($scene['Scene']['name']); ?>&nbsp;</td>
							<td>
								<?php echo $this->Html->link($scene['Room']['name'], array('controller' => 'rooms', 'action' => 'view', $scene['Room']['id'])); ?>
							</td>
							<td id="scene_current_status_<?php echo h($scene['Scene']['id']); ?>">&nbsp;</td>
							<td><?php echo h($scene['Scene']['on_dashboard']); ?>&nbsp;</td>
							<td><?php echo h($scene['Scene']['created']); ?>&nbsp;</td>
							<td><?php echo h($scene['Scene']['modified']); ?>&nbsp;</td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('View'), array('action' => 'view', $scene['Scene']['id']), array('class' => 'btn btn-default btn-xs')); ?>
									<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $scene['Scene']['id']), array('class' => 'btn btn-default btn-xs')); ?>
									<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $scene['Scene']['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $scene['Scene']['id'])); ?>
									<?php
										echo $this->Js->link(
										    'Run',
										    array('action' => 'run', $scene['Scene']['id']),
										    array('async' => true, 
										    	  'htmlAttributes' => array('class' => 'btn btn-default btn-xs')
										    	)
										);

										echo $this->Js->writeBuffer();
									?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			<p><small>
				<?php
					echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					));
				?>
			</small></p>

			<ul class="pagination">
				<?php
					echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
					echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
					echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				?>
			</ul><!-- /.pagination -->
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->