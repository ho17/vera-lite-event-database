<script type="text/javascript">
	var showStatus = function( id, statusValue, statusText ) {
		var c = (statusValue === 0) ? '#F2DEDE' : '#DFF0D8';
		var previousStatusText = $('#scene_previous_status_' + id).text();
		if (previousStatusText != statusText) {
			$('#scene_previous_status_' + id).html(statusText);
			$('#scene_current_status_' + id).html(statusText);
			$('#scene_current_status_' + id).effect("highlight", {color: c}, 3500);
		}
	};

	<?php
		foreach ($scenesActive as $id => $active) {
	?>
			showStatus(<?php echo $id; ?>, 
						<?php echo ($active ? "1" : "0") ?>, 
						'<?php echo $active ? "Active" : "Inactive"; ?>'
						);
	<?php
		}
	?>

</script>
