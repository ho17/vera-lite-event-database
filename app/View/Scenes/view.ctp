<script type="text/javascript">
	$( document ).ready(function() {
		updateActive();
		<?php if (!empty($events)): ?>
			updateGraph();
		<?php endif; ?>

		// update device status regularly
		$('#progress').polartimer({
		 timerSeconds: <?php echo REFRESH_ACTIVE; ?>,
		 color: '#DDDDDD',
		 opacity: 0.5,
		 callback: function () {
			updateActive();
			$('#progress').polartimer('start');
		 }
		});
		$('#progress').polartimer('start');
	});

	var updateActive = function() {
	    <?php
			echo $this->Js->request(
		        array('controller' => 'scenes', 'action' => 'status', h($scene['Scene']['id'])),
		        array('async' => true,
		        	  'update' => '#scenes_success',
		        	));
		?>
	};

</script>

<div class='hidden' id='scenes_success'></div>
<div class='hidden' id='scene_previous_status_<?php echo h($scene['Scene']['id']); ?>'></div>

<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">

		<?php echo $this->element('menu/view_scene'); ?>

		<div id="progress"></div>
		
	</div><!-- /#sidebar .span3 -->
	
	<div id="page-content" class="col-sm-9">
		
		<div class="scenes view">

			<div class="page-header">
			  <h2><?php echo __('Scene'); ?></h2>
			</div>
			
			<div class="table-responsive">
				<table class="table table-hover table-striped table-bordered <?php if (TABLE_CONDENSED) { echo 'table-condensed'; } ?>">
					<tbody>
						<tr>
							<td><strong><?php echo __('Id'); ?></strong></td>
							<td>
								<?php echo h($scene['Scene']['id']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Name'); ?></strong></td>
							<td>
								<?php echo h($scene['Scene']['name']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Room'); ?></strong></td>
							<td>
								<?php echo $this->Html->link($scene['Room']['name'], array('controller' => 'rooms', 'action' => 'view', $scene['Room']['id']), array('class' => '')); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Show on dashboard'); ?></strong></td>
							<td>
								<?php echo h($scene['Scene']['on_dashboard']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Created'); ?></strong></td>
							<td>
								<?php echo h($scene['Scene']['created']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Modified'); ?></strong></td>
							<td>
								<?php echo h($scene['Scene']['modified']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td><strong><?php echo __('Status'); ?></strong></td>
							<td id="scene_current_status_<?php echo h($scene['Scene']['id']); ?>">
								&nbsp;
							</td>
						</tr>
					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

		<div class="actions">
			<?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Scene'), array('controller' => 'scenes', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>

			<?php
				echo $this->Js->link(
				    'Run',
				    array('action' => 'run', $scene['Scene']['id']),
				    array('async' => true, 
				    	  'htmlAttributes' => array('class' => 'btn btn-primary')
				    	)
				);

				echo $this->Js->writeBuffer();
			?>
		</div><!-- /.actions -->
			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->