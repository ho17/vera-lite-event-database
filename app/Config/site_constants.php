<?php
	define('VERA_LITE_IP', '192.168.178.20');
	define('EVENTS_PER_PAGE', 30);
	define('DEVICES_PER_PAGE', 30);
	define('ROOMS_PER_PAGE', 30);
	define('CATEGORIES_PER_PAGE', 30);
	define('SECTIONS_PER_PAGE', 30);
	define('TABLE_CONDENSED', true);
	define('REFRESH_STATUS', 5); // in seconds
	define('REFRESH_ACTIVE', 5); // in seconds

	define('CAT_DIMMABLE_LIGHT', 2);
	define('CAT_SWITCH', 3);
	define('CAT_SENSOR', 4);
	define('CAT_WINDOW_COVERING', 8);
	define('CAT_HUMIDITY_SENSOR', 16);
	define('CAT_TEMPERATURE_SENSOR', 17);

	define('DIMMABLE_LIGHT_STEP_SIZE', 10);