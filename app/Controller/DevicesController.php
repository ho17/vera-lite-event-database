<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::import('Controller', 'rooms');
App::import('Controller', 'events');
/**
 * Devices Controller
 *
 * @property Device $Device
 * @property PaginatorComponent $Paginator
 */
class DevicesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public $helpers = array('Js');

    public $paginate = array(
        'limit' => DEVICES_PER_PAGE,
        'order' => array(
            'Device.id' => 'asc'
        )
    );

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Device->recursive = 0;
		$this->Paginator->settings = $this->paginate;
		$this->set('devices', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Device->exists($id)) {
			throw new NotFoundException(__('Invalid device'));
		}
		$options = array('conditions' => array('Device.' . $this->Device->primaryKey => $id));
		$this->Device->recursive = 0; // Don't get related events
		$device = $this->Device->find('first', $options);
		$this->set('device', $device);

		$events = new EventsController();		
		$totalCount = $events->_getTotalCount($id);
		$dayCount = $events->_getDayCount($id);
		$weekCount = $events->_getWeekCount($id);
		$monthCount = $events->_getMonthCount($id);
		$yearCount = $events->_getYearCount($id);

		$this->set('totalCount', $totalCount);
		$this->set('dayCount', $dayCount);
		$this->set('weekCount', $weekCount);
		$this->set('monthCount', $monthCount);
		$this->set('yearCount', $yearCount);

		// Get related events for this device
		$this->Device->Event->recursive = -1;
		$this->Paginator->settings = array(
	        'limit' => EVENTS_PER_PAGE,
	        'order' => array(
	            'Event.created' => 'desc'
	        ));
		$events = $this->paginate($this->Device->Event, array('Event.device_id' => $id));
		$this->set(compact('events'));
	}

	public function refresh() { // call from the browser
		if ($this->_refresh()) {
			$this->Session->setFlash(__('Devices refreshed'), 'flash/success');
		} else {
			$this->Session->setFlash(__('Devices not refreshed'), 'flash/error');
		}
		$this->redirect(array('action' => 'index'));
	}

/**
 * method to refresh the list with devices
 *
 * @return void
 */
 	public function _refresh($response = null) {
 		if ($response==null) {
			$HttpSocket = new HttpSocket();
			$url = 'http://' . VERA_LITE_IP . ':3480/data_request';
			$data = array(
					'id' => 'user_data',
					'output_format'=>'json'
					);
			$response = $HttpSocket->get($url, $data);
		}
		if (!empty($response)) {
			$response = json_decode($response, true);

			$this->Device->query('TRUNCATE devices;');
			foreach ($response['devices'] as $device) {
				if (isset($device['invisible']) && $device['invisible']==1) {
					continue;
				}
				$data['id'] = $device['id'];
				$data['name'] = $device['name'];
				$data['room_id'] = $device['room'];
				$data['category_id'] = (isset($device['category_num'])) ? $device['category_num'] : 0;
				$data['on_dashboard'] = (isset($device['onDashboard']) && ($device['onDashboard']==1));
				$this->Device->save($data);
			}

			return true;
		} else {
			return false;
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Device->create();
			if ($this->Device->save($this->request->data)) {
				$this->Session->setFlash(__('The device has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The device could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$rooms = $this->Device->Room->find('list');
		$this->set(compact('rooms'));
		$categories = $this->Device->Category->find('list');
		$this->set(compact('categories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Device->id = $id;
		if (!$this->Device->exists($id)) {
			throw new NotFoundException(__('Invalid device'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Device->save($this->request->data)) {
				$this->Session->setFlash(__('The device has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The device could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Device.' . $this->Device->primaryKey => $id));
			$this->request->data = $this->Device->find('first', $options);
		}
		$rooms = $this->Device->Room->find('list');
		$this->set(compact('rooms'));
		$categories = $this->Device->Category->find('list');
		$this->set(compact('categories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Device->id = $id;
		if (!$this->Device->exists()) {
			throw new NotFoundException(__('Invalid device'));
		}
		if ($this->Device->delete()) {
			$this->Session->setFlash(__('Device deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Device was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}

	public function today($id = null) {
		if (!$this->Device->exists($id)) {
			throw new NotFoundException(__('Invalid device'));
		}
		/*
		$options = array('conditions' => array('Device.' . $this->Device->primaryKey => $id));
		$this->set('device', $this->Device->find('first', $options));
		*/

		$events = new EventsController();
		//$todayEvents = $events->getEvents($id, $start, $end);
		$todayEvents = $events->getEvents($id);

		$this->set('events', $todayEvents);
		$this->set('deviceId', $id);
		$this->render('graph', 'ajax');
	}

	public function status($id = null) {
		//http://vera:3480/data_request?id=lu_status2&output_format=json
		$HttpSocket = new HttpSocket();
		$url = 'http://' . VERA_LITE_IP . ':3480/data_request';
		$data = array(
				'id' => 'lu_status2',
				'output_format' => 'json'
				);
		$response = $HttpSocket->get($url, $data);
		if (!empty($response)) {
			$status = array();
			$response = json_decode($response, true);
			foreach ($response['devices'] as $respDevice) {
				/*
				if ($id!=null && $device['id']==$id) {
					// only save status of requested device id
					//$status[$devices['id']] = $devices[''];
				} elseif ($id==null) {
					// get status of all devices
					$status[$device['id']] = $device[''];
				}
				*/
				if (!$this->Device->exists($respDevice['id'])) {
					continue;
				}
				$this->Device->id = $respDevice['id'];
				$options = array('conditions' => array('Device.' . $this->Device->primaryKey => $respDevice['id']));
				$dbDevice = $this->Device->find('first', $options);
				
				$status[$respDevice['id']]['categoryId'] = $dbDevice['Category']['id'];
				$status[$respDevice['id']]['statusLevel'] = 0;
				$status[$respDevice['id']]['status'] 	  = 'Undefined';
				
				foreach ($respDevice['states'] as $state) {

					if ((($dbDevice['Category']['id'] == CAT_DIMMABLE_LIGHT) || ($dbDevice['Category']['id'] == CAT_WINDOW_COVERING))
						&& ($state['variable'] == 'LoadLevelStatus')) {
						// Dimmable Light or Window Covering status level
						$status[$respDevice['id']]['statusLevel'] = $state['value'];
						$status[$respDevice['id']]['status'] 	  = $state['value'];
						break;
					} elseif (($dbDevice['Category']['id'] == CAT_WINDOW_COVERING) && ($state['variable'] == 'Status')) {
						// Window Covering status
						$status[$respDevice['id']]['status'] = $state['value'];
						break;
					} elseif (($dbDevice['Category']['id'] == CAT_SWITCH) && ($state['variable'] == 'Status')) {
						// Switch status
						$status[$respDevice['id']]['status'] = $state['value'];
						break;
					} elseif (($dbDevice['Category']['id'] == CAT_SENSOR) && ($state['variable'] == 'Tripped')) {
						// Sensor status
						$status[$respDevice['id']]['status'] = $state['value'];
						break;
					} elseif (($dbDevice['Category']['id'] == CAT_HUMIDITY_SENSOR) && ($state['variable'] == 'CurrentLevel')) {
						// Humidity sensor status
						$status[$respDevice['id']]['status'] = $state['value'];
						break;
					} elseif (($dbDevice['Category']['id'] == CAT_TEMPERATURE_SENSOR) && ($state['variable'] == 'CurrentTemperature')) {
						// Temperature sensor status
						$status[$respDevice['id']]['status'] = $state['value'];
						break;
					} elseif (($state['service'] == 'urn:rts-services-com:serviceId:DayTime') && ($state['variable'] == 'Status')) {
						// DayTime plugin status
						$status[$respDevice['id']]['status'] = $state['value'];
						break;
					} elseif (($state['service'] == 'urn:upnp-org:serviceId:VSwitch1') && ($state['variable'] == 'Status')) {
						// Virtual switch plugin status
						$status[$respDevice['id']]['status'] = $state['value'];
					}
				}	
			}
		}

		if ($id != null) {
			$s = $status[$id];
			$status = null;
			$status[$id] = $s;
		}

		$this->set('devicesStatus', $status);
		$this->set('deviceId', $id);
		$this->render('status', 'ajax');
	}

	public function toggle($id = null, $targetValue = null) {
		if (!$this->Device->exists($id)) {
			throw new NotFoundException(__('Invalid device'));
		}

		if ($targetValue == null) {
			$this->status($id);
			return;
		}

		$this->Device->id = $id;
		$options = array('conditions' => array('Device.' . $this->Device->primaryKey => $id));
		$device = $this->Device->find('first', $options);

		$data = array(
				'id' => 'lu_action',
				'output_format' => 'json',
				'DeviceNum' => $id,
				);

		switch ($device['Category']['id']) {
			case CAT_DIMMABLE_LIGHT:
				$data['serviceId'] = 'urn:upnp-org:serviceId:Dimming1';
				$data['action'] = 'SetLoadLevelTarget';
				$data['newLoadlevelTarget'] = $targetValue;
				break;
			case CAT_WINDOW_COVERING:
			case CAT_SWITCH:
				$data['serviceId'] = 'urn:upnp-org:serviceId:SwitchPower1';
				$data['action'] = 'SetTarget';
				$data['newTargetValue'] = $targetValue;
				break;
			default:
				# no service id by device category
				$this->status($id); // only return requires a view
				return;
		}

		$url = 'http://' . VERA_LITE_IP . ':3480/data_request';
		$HttpSocket = new HttpSocket();
		$response = $HttpSocket->get($url, $data);

		$this->autoRender = false;
	}

	public function dashboard() {
		$this->Device->recursive = 0;
		$this->Paginator->settings = array(
			'limit' => $this->Device->find('count'), // Essentially disable pagination
			'conditions' => array('Device.on_dashboard =' => '1'),
	        'order' => array(
		            'Device.room_id' => 'asc',
		            'Device.name' => 'asc'
		        ));
		$this->set('devices', $this->paginate());
	}
}
