<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::import('Controller', 'sections');
/**
 * Rooms Controller
 *
 * @property Room $Room
 * @property PaginatorComponent $Paginator
 */
class RoomsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

    public $paginate = array(
        'limit' => ROOMS_PER_PAGE,
        'order' => array(
            'Room.id' => 'asc'
        )
    );

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Room->recursive = 0;
		$this->Paginator->settings = $this->paginate;
		$this->set('rooms', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Room->exists($id)) {
			throw new NotFoundException(__('Invalid room'));
		}
		$options = array('conditions' => array('Room.' . $this->Room->primaryKey => $id));
		$this->set('room', $this->Room->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Room->create();
			if ($this->Room->save($this->request->data)) {
				$this->Session->setFlash(__('The room has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The room could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

	public function refresh() { // call from the browser
		if ($this->_refresh()) {
			$this->Session->setFlash(__('Rooms refreshed'), 'flash/success');
		} else {
			$this->Session->setFlash(__('Rooms not refreshed'), 'flash/error');
		}
		$this->redirect(array('action' => 'index'));
	}

/**
 * method to refresh the list with rooms
 *
 * @return void
 */
 	public function _refresh($response = null) {
 		if ($response==null) {
			$HttpSocket = new HttpSocket();
			$url = 'http://' . VERA_LITE_IP . ':3480/data_request';
			$data = array(
					'id' => 'user_data',
					'output_format'=>'json'
					);
			$response = $HttpSocket->get($url, $data);
		}
		if (!empty($response)) {
			$response = json_decode($response, true);

			$this->Room->query('TRUNCATE rooms;');
			foreach ($response['rooms'] as $room) {
				$data['id'] = $room['id'];
				$data['name'] = $room['name'];
				$data['section_id'] = $room['section'];
				$this->Room->save($data);
			}

			return true;
		} else {
			return false;
		}
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Room->id = $id;
		if (!$this->Room->exists($id)) {
			throw new NotFoundException(__('Invalid room'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Room->save($this->request->data)) {
				$this->Session->setFlash(__('The room has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The room could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Room.' . $this->Room->primaryKey => $id));
			$this->request->data = $this->Room->find('first', $options);
		}
		$sections = $this->Room->Section->find('list');
		$this->set(compact('sections'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Room->id = $id;
		if (!$this->Room->exists()) {
			throw new NotFoundException(__('Invalid room'));
		}
		if ($this->Room->delete()) {
			$this->Session->setFlash(__('Room deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Room was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}}
