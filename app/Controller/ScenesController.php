<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::import('Controller', 'rooms');

/**
 * Scenes Controller
 *
 * @property Scene $Scene
 * @property PaginatorComponent $Paginator
 */
class ScenesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Scene->recursive = 0;
		$this->set('scenes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Scene->exists($id)) {
			throw new NotFoundException(__('Invalid scene'));
		}
		$options = array('conditions' => array('Scene.' . $this->Scene->primaryKey => $id));
		$this->set('scene', $this->Scene->find('first', $options));
	}

	public function refresh() { // call from the browser
		if ($this->_refresh()) {
			$this->Session->setFlash(__('Scenes refreshed'), 'flash/success');
		} else {
			$this->Session->setFlash(__('Scenes not refreshed'), 'flash/error');
		}
		$this->redirect(array('action' => 'index'));
	}

/**
 * method to refresh the list with scenes
 *
 * @return void
 */
 	public function _refresh($response = null) {
 		if ($response==null) {
			$HttpSocket = new HttpSocket();
			$url = 'http://' . VERA_LITE_IP . ':3480/data_request';
			$data = array(
					'id' => 'user_data',
					'output_format'=>'json'
					);
			$response = $HttpSocket->get($url, $data);
		}
		if (!empty($response)) {
			$response = json_decode($response, true);

			$this->Scene->query('TRUNCATE scenes;');
			foreach ($response['scenes'] as $scene) {
				if (isset($scene['notification_only'])) {
					continue;
				}
				$data['id'] = $scene['id'];
				$data['name'] = $scene['name'];
				$data['room_id'] = $scene['room'];
				$data['on_dashboard'] = isset($scene['onDashboard']) && ($scene['onDashboard']==1);
				$this->Scene->save($data);
			}

			return true;
		} else {
			return false;
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Scene->create();
			if ($this->Scene->save($this->request->data)) {
				$this->Session->setFlash(__('The scene has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The scene could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$rooms = $this->Scene->Room->find('list');
		$this->set(compact('rooms'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Scene->id = $id;
		if (!$this->Scene->exists($id)) {
			throw new NotFoundException(__('Invalid scene'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Scene->save($this->request->data)) {
				$this->Session->setFlash(__('The scene has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The scene could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Scene.' . $this->Scene->primaryKey => $id));
			$this->request->data = $this->Scene->find('first', $options);
		}
		$rooms = $this->Scene->Room->find('list');
		$this->set(compact('rooms'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Scene->id = $id;
		if (!$this->Scene->exists()) {
			throw new NotFoundException(__('Invalid scene'));
		}
		if ($this->Scene->delete()) {
			$this->Session->setFlash(__('Scene deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Scene was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}

	public function run($id = null) {
		if (!$this->Scene->exists($id)) {
			throw new NotFoundException(__('Invalid scene'));
		}
		$this->Scene->id = $id;
		$options = array('conditions' => array('Scene.' . $this->Scene->primaryKey => $id));
		$scene = $this->Scene->find('first', $options);

		//vera:3480/data_request?id=lu_action&serviceId=urn:micasaverde-com:serviceId:HomeAutomationGateway1&action=RunScene&SceneNum=
		$HttpSocket = new HttpSocket();
		$url = 'http://' . VERA_LITE_IP . ':3480/data_request';
		$data = array(
				'id' => 'lu_action',
				'output_format' => 'json',
				'SceneNum' => $id,
				'serviceId' => 'urn:micasaverde-com:serviceId:HomeAutomationGateway1',
				'action' => 'RunScene'
				);
		$response = $HttpSocket->get($url, $data);

		$this->autoRender = false;
	}

	public function status($id = null) {
		//http://vera:3480/data_request?id=lu_status2&output_format=xml
		if ($id==null) {
			echo "blaabl";
		}
		$HttpSocket = new HttpSocket();
		$url = 'http://' . VERA_LITE_IP . ':3480/data_request';
		$data = array(
				'id' => 'lu_status2',
				'output_format' => 'json'
				);
		$response = $HttpSocket->get($url, $data);
		$active = array();
		if (!empty($response)) {
			$response = json_decode($response, true);
			foreach ($response['scenes'] as $scene) {
				if ($id!=null && $scene['id']==$id) {
					// only save status of requested scene id
					$active[$scene['id']] = $scene['active'];
				} elseif ($id==null) {
					// get status of all scenes
					$active[$scene['id']] = $scene['active'];
				}
			}
		}

		$this->set('scenesActive', $active);
		$this->set('sceneId', $id);
		$this->render('status', 'ajax');
	}

	public function dashboard() {
		$this->Scene->recursive = 0;
		$this->Paginator->settings = array(
			'limit' => $this->Scene->find('count'), // Essentially disable pagination
			'conditions' => array('Scene.on_dashboard =' => '1'),
	        'order' => array(
		            'Scene.room_id' => 'asc',
		            'Scene.name' => 'asc'
		        ));
		$this->set('scenes', $this->paginate());
	}

}
