<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::import('Controller', 'Devices');
App::import('Controller', 'Rooms');
App::import('Controller', 'Categories');
App::import('Controller', 'Scenes');

/**
 * Sections Controller
 *
 * @property Section $Section
 * @property PaginatorComponent $Paginator
 */
class SectionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

    public $paginate = array(
        'limit' => SECTIONS_PER_PAGE,
        'order' => array(
            'Section.id' => 'asc'
        )
    );

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Section->recursive = 0;
		$this->Paginator->settings = $this->paginate;
		$this->set('sections', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Section->exists($id)) {
			throw new NotFoundException(__('Invalid section'));
		}
		$options = array('conditions' => array('Section.' . $this->Section->primaryKey => $id));
		$this->set('section', $this->Section->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Section->create();
			if ($this->Section->save($this->request->data)) {
				$this->Session->setFlash(__('The section has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The section could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

	public function refresh() { // call from the browser
		if ($this->_refresh()) {
			$this->Session->setFlash(__('Sections refreshed'), 'flash/success');
		} else {
			$this->Session->setFlash(__('Sections not refreshed'), 'flash/error');
		}
		$this->redirect(array('action' => 'index'));
	}

	public function refreshAll() { // call from the browser
		if ($this->_refreshAll()) {
			$this->Session->setFlash(__('Sections, devices, rooms, categories & scenes refreshed'), 'flash/success');
		} else {
			$this->Session->setFlash(__('Sections, devices, rooms, categories & scenes not refreshed'), 'flash/error');
		}
		$this->redirect(array('action' => 'index'));
	}

	public function _refreshAll() {
		$HttpSocket = new HttpSocket();
		$url = 'http://' . VERA_LITE_IP . ':3480/data_request';
		$data = array(
				'id' => 'user_data',
				'output_format'=>'json'
				);
		$response = $HttpSocket->get($url, $data);
		if (!empty($response)) {

			$devices = new DevicesController();
			$rooms = new RoomsController();
			$categories = new CategoriesController();
			$scenes = new ScenesController();

			return $this->_refresh($response) &&
				   $devices->_refresh($response) && 
				   $rooms->_refresh($response) &&
				   $scenes->_refresh($response) &&
				   $categories->_refresh();
		} else {
			return false;
		}
	}

/**
 * method to refresh the list with sectiona
 *
 * @return void
 */
 	public function _refresh($response = null) {
 		if ($response==null) {
			$HttpSocket = new HttpSocket();
			$url = 'http://' . VERA_LITE_IP . ':3480/data_request';
			$data = array(
					'id' => 'user_data',
					'output_format'=>'json'
					);
			$response = $HttpSocket->get($url, $data);
		}
		if (!empty($response)) {
			$response = json_decode($response, true);

			$this->Section->query('TRUNCATE sections;');
			foreach ($response['sections'] as $section) {
				$data['id'] = $section['id'];
				$data['name'] = $section['name'];
				$this->Section->save($data);
			}

			return true;
		} else {
			return false;
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Section->id = $id;
		if (!$this->Section->exists($id)) {
			throw new NotFoundException(__('Invalid section'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Section->save($this->request->data)) {
				$this->Session->setFlash(__('The section has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The section could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Section.' . $this->Section->primaryKey => $id));
			$this->request->data = $this->Section->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Section->id = $id;
		if (!$this->Section->exists()) {
			throw new NotFoundException(__('Invalid section'));
		}
		if ($this->Section->delete()) {
			$this->Session->setFlash(__('Section deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Section was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}}
