<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
/**
 * Categories Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 */
class CategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public $helpers = array('Js');

    public $paginate = array(
        'limit' => CATEGORIES_PER_PAGE,
        'order' => array(
            'Category.id' => 'asc'
        )
    );

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Category->recursive = 0;
		$this->Paginator->settings = $this->paginate;
		$this->set('categories', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
		$this->set('category', $this->Category->find('first', $options));
		$rooms = $this->Category->Device->Room->find('list');
		$this->set(compact('rooms'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Category->create();
			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

	public function refresh() { // call from the browser
		if ($this->_refresh()) {
			$this->Session->setFlash(__('Categories refreshed'), 'flash/success');
		} else {
			$this->Session->setFlash(__('Categories not refreshed'), 'flash/error');
		}
		$this->redirect(array('action' => 'index'));
	}

/**
 * method to refresh the list with categories
 *
 * @return void
 */
 	public function _refresh() {
		$HttpSocket = new HttpSocket();
		$url = 'http://' . VERA_LITE_IP . ':3480/data_request';
		$data = array(
				'id' => 'sdata',
				'output_format'=>'json'
				);
		$response = $HttpSocket->get($url, $data);
		if (!empty($response)) {
			$response = json_decode($response, true);

			$this->Category->query('TRUNCATE categories;');
			foreach ($response['categories'] as $category) {
				$data['id'] = $category['id'];
				$data['name'] = $category['name'];
				$this->Category->save($data);
			}

			return true;
		} else {
			return false;
		}
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Category->id = $id;
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
			$this->request->data = $this->Category->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->Category->delete()) {
			$this->Session->setFlash(__('Category deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Category was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}}
