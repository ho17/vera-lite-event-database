<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::import('Controller', 'Devices');
App::import('Controller', 'Sections');
/**
 * Events Controller
 *
 * @property Event $Event
 * @property PaginatorComponent $Paginator
 */
class EventsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

    public $paginate = array(
        'limit' => EVENTS_PER_PAGE,
        'order' => array(
            'Event.created' => 'desc'
        )
    );

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Event->recursive = 0;
		$this->Paginator->settings = $this->paginate;
		$this->set('events', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Event->exists($id)) {
			throw new NotFoundException(__('Invalid event'));
		}
		$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
		$this->set('event', $this->Event->find('first', $options));
	}

	//public function getEvents($deviceId, $start, $end) {
	public function getEvents($deviceId) {
		$options = array(
				'conditions' => array(
						'Event.device_id' => $deviceId,
						//'Event.created' => 'CURDATE()'
						'DAY(Event.created)' => date('d')
						)
				);
		/*
		$this->set('events', $this->Event->find('all', $options));
		$this->render('index');
		*/
		return $this->Event->find('all', $options);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Event->create();
			if ($this->Event->save($this->request->data)) {
				$this->Session->setFlash(__('The event has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The event could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$devices = $this->Event->Device->find('list');
		$this->set(compact('devices'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Event->id = $id;
		if (!$this->Event->exists($id)) {
			throw new NotFoundException(__('Invalid event'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Event->save($this->request->data)) {
				$this->Session->setFlash(__('The event has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The event could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
			$this->request->data = $this->Event->find('first', $options);
		}
		$devices = $this->Event->Device->find('list');
		$this->set(compact('devices'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Event->id = $id;
		if (!$this->Event->exists()) {
			throw new NotFoundException(__('Invalid event'));
		}
		if ($this->Event->delete()) {
			$this->Session->setFlash(__('Event deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Event was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}

	public function vera() {
		if ($this->request->is('get')) {
			// https://VERA_LITE_IP/alert?PK_AccessPoint=35106193&HW_Key=QW03G6iXLqkCdh20qaYUjnENyY3n57Og&DeviceID=4&LocalDate=2013-12-06 15:03:05&LocalTimestamp=1386338585&AlertType=3&SourceType=3&Argument=8&Format=&Code=Status&Value=0&Description=Computerlamp uit&Users=
			// https://VERA_LITE_IP/alert
			// ?PK_AccessPoint=35106193
			// &HW_Key=QW03G6iXLqkCdh20qaYUjnENyY3n57Og
			// &DeviceID=4
			// &LocalDate=2013-12-06 15:03:05
			// &LocalTimestamp=1386338585
			// &AlertType=3
			// &SourceType=3
			// &Argument=8
			// &Format=
			// &Code=Status
			// &Value=0
			// &Description=Computerlamp uit
			// &Users=
			
			$devices = new DevicesController();
			if (!$devices->Device->exists($this->request->query['DeviceID'])) {
				// An event of an unknown device occurred, so refresh all data (i.e. sections, categories, devices & rooms)
				$sections = new SectionsController();
				$sections->_refreshAll();
			}

			$data['device_id'] = $this->request->query['DeviceID'];
			$data['description'] = $this->request->query['Description'];
			$data['value'] = $this->request->query['Value'];
			$data['created'] = $this->request->query['LocalDate'];
			$this->Event->create();
			if ($this->Event->save($data)) {

			    $this->response->type('');
				/*
				$this->response->header(array(
					'Content-Type: ',
					'HTTP/1.1 200 OK',
					'Content-Length: 10'
				));
				*/
				$this->autoLayout = false;
				$this->response->body("PK_Alert:0"); //Vera expects this
				return $this->response;
			} else {
				// Event could not be saved
			}
		}
	}

	public function _getTotalCount($deviceId) {
		$options = array(
				'conditions' => array(
						'Event.device_id' => $deviceId,
						'Event.value' => 1,
						)
				);

		return $this->Event->find('count', $options);	
	}

	public function _getDayCount($deviceId) {
		$options = array(
				'conditions' => array(
						'Event.device_id' => $deviceId,
						'Event.value' => 1,
						'DAYOFMONTH(Event.created)' => date('d')
						)
				);

		return $this->Event->find('count', $options);	
	}

	public function _getWeekCount($deviceId) {
		$options = array(
				'conditions' => array(
						'Event.device_id' => $deviceId,
						'Event.value' => 1,
						'WEEKOFYEAR(Event.created)' => date('W')
						)
				);

		return $this->Event->find('count', $options);	
	}

	public function _getMonthCount($deviceId) {
		$options = array(
				'conditions' => array(
						'Event.device_id' => $deviceId,
						'Event.value' => 1,
						'MONTH(Event.created)' => date('n')
						)
				);

		return $this->Event->find('count', $options);	
	}

	public function _getYearCount($deviceId) {
		$options = array(
				'conditions' => array(
						'Event.device_id' => $deviceId,
						'Event.value' => 1,
						'YEAR(Event.created)' => date('Y')
						)
				);

		return $this->Event->find('count', $options);	
	}
}