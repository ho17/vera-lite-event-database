Vera Lite device events logging based on CakePHP with Bootstrap
===============================================================

Pre-requisites:
---------------
- Vera Lite.
- Webserver with https listening.
- MySQL database.

Setup:
------
- Create database user (i.e. Vera)
- Create database (i.e. Vera)
- Import vera.sql
- Copy app\Config\database.php.default to app\Config\database.php
- Change database 'default' settings (host, login, password, database)
- Change url / ip of your Vera Lite in app\Config\site_constants.php
- Make app\tmp\ writable for the webserver
- Add [AlternateEventServer](http://wiki.micasaverde.com/index.php/AlternateEventServer) to Vera Lite
```
http://<your.vera.ip.address>:3480/data_request?id=variableset&Variable=AltEventServer&Value=<your.webserver.ip.address>
```
- Click Reload in Vera's UI
- Add notifications to devices in Vera's UI
- Vhost
```
    <IfModule mod_ssl.c>
    <VirtualHost _default_:443>
        ServerAdmin webmaster@localhost

        DocumentRoot /home/user/www/cake_vera_events
        <Directory />
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order Allow,Deny
            Allow from all
        </Directory>
	</VirtualHost>
	</IfModule>
```

Done (I hope):
-------------
Point your browser to [https://<your.webserver.ip.address>](https://<your.webserver.ip.address>)

Credits:
--------
[Quinten's post](http://forum.micasaverde.com/index.php?topic=15245.0)
